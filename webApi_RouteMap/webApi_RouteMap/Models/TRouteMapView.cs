﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class TRouteMapView
    {
        public IEnumerable<TNode> nodeList { get; set; }
        public IEnumerable<TRoute> neighborList { get; set; }
        public IEnumerable<string> inputGraphList { get; set; }
    }
}