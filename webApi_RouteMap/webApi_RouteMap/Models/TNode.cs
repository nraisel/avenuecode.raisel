﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class TNode
    {
        public string name { get; set; }
        public bool visited { get; set; }
    }
}