﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class TEdge
    {
        public int weight { get; set; }
        public TNode destinationNode { get; set; }
    }
}