﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webApi_RouteMap.Models
{
    public interface IInputGraph
    {
        IEnumerable<string> getPairOfNodeList(string route);
    }
}
