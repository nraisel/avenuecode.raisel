﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class TMapInit : IMap, IInputGraph
    {
        public TRouteMapView createRouteMap(object input)
        {
            IHelper hlp = new THelper();
            IEnumerable<TNode> nodeList = hlp.getNodeList(input);
            IEnumerable<TRoute> neighborList = hlp.getNeighborList(input);
            IEnumerable<string> inputGraphList = hlp.getInputGraphList(input);
            return new TRouteMapView { nodeList = nodeList, neighborList = neighborList, inputGraphList = inputGraphList };
        }

        public IEnumerable<string> getPairOfNodeList(string route)
        {
            IInputGraph hlp = new THelper();
            return hlp.getPairOfNodeList(route);
        }
    }
}