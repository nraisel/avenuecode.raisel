﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webApi_RouteMap.Models
{
    public interface IHelper
    {
        IEnumerable<TNode> getNodeList(object inputGraph);
        IEnumerable<TRoute> getNeighborList(object inputGraph);
        IEnumerable<string> getInputGraphList(object inputGraph);

        
    }
}
