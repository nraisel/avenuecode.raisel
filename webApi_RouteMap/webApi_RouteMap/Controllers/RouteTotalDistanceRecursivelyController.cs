﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webApi_RouteMap.Models;

namespace webApi_RouteMap.Controllers
{
    public class RouteTotalDistanceRecursivelyController : ApiController
    {
        public TAnswer GetTotalDistance(string routeGraph)
        {
            IRouteMap routeMap = new TRouteMap();
            return routeMap.getTotalDistance_recursively(routeGraph);
        }
    }
}
