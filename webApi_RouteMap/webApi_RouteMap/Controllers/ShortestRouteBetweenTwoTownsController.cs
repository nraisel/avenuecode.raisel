﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webApi_RouteMap.Models;

namespace webApi_RouteMap.Controllers
{
    public class ShortestRouteBetweenTwoTownsController : ApiController
    {
        public TAnswer GetShortestRouteBetweenTwoTowns(string routeGraph)
        {
            IRouteMap routeMap = new TRouteMap();
            return routeMap.getShortestRoute(routeGraph);
        }
    }
}
