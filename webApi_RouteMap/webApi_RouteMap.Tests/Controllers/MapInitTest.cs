﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webApi_RouteMap.Models;

namespace webApi_RouteMap.Tests.Controllers
{
    [TestClass]
    public class MapInitTest
    {
        [TestMethod]
        public void Test_to_verify_Initialization_of_the_input_graph()
        {
            // Arrange
            IMap mapObj = new TMapInit();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

            // Act
            TRouteMapView result = mapObj.createRouteMap(inputGraph);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.nodeList.Count());
        }

        [TestMethod]
        public void Test_to_verify_getPairOfNodeList_method()
        {
            // Arrange
            IInputGraph mapObj = new TMapInit();
            string route = "A-E-B-C-D";

            // Act
            IEnumerable<string> result = mapObj.getPairOfNodeList(route);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(60, result.Count());
        }
    }
}
