﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webApi_RouteMap.Models;

namespace webApi_RouteMap.Tests.Controllers
{
    [TestClass]
    public class HelperTest
    {
        [TestMethod]
        public void Test_to_verify_method_getInputGraphList_returns_array_of_strings()
        {
            // Arrange
            IHelper helperObj = new THelper();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

            // Act
            IEnumerable<string> result = helperObj.getInputGraphList(inputGraph);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        public void Test_to_verify_method_getNodeList_returns_a_list_of_TNode_objects()
        {
            // Arrange
            IHelper helperObj = new THelper();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

            // Act
            IEnumerable<TNode> result = helperObj.getNodeList(inputGraph);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        public void Test_to_verify_method_getNeighborList_a_list_of_TRoute_objects()
        {
            // Arrange
            IHelper helperObj = new THelper();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

            // Act
            IEnumerable<TRoute> result = helperObj.getNeighborList(inputGraph);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("A", result.ElementAt(0).originNode.name);
            Assert.AreEqual(3, result.ElementAt(0).listOfNodeNeighbors.Count());

            Assert.AreEqual("B", result.ElementAt(1).originNode.name);
            Assert.AreEqual(1, result.ElementAt(1).listOfNodeNeighbors.Count());

            Assert.AreEqual("C", result.ElementAt(2).originNode.name);
            Assert.AreEqual(2, result.ElementAt(2).listOfNodeNeighbors.Count());
        }
    }
}
