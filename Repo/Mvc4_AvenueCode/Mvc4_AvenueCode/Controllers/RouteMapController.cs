﻿using Mvc4_AvenueCode.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mvc4_AvenueCode.Controllers
{
    public class RouteMapController : Controller
    {
        //
        // GET: /RouteMap/

        public async Task<ActionResult> RouteTotalDistanceRecursively(string inputGraph, string route)
        {
            var input = route + "Y" + inputGraph;
            var routeMapSrv = new RouteMapService();
            var result = await routeMapSrv.GetRouteTotalDistanceRecursivelyAsync(input);
            if (Request.IsAjaxRequest())
                return PartialView("_pwRouteTotalDistance", result);
            else
                return View(result);
        }

        public async Task<ActionResult> ShortestRouteBetweenTwoTowns(string inputGraph, string route)
        {
            var input = route + "Y" + inputGraph;
            var routeMapSrv = new RouteMapService();
            var result = await routeMapSrv.GetShortestRouteBetweenTwoTownsAsync(input);
            if (Request.IsAjaxRequest())
                return PartialView("_pwShortestRouteBetweenTwoTowns", result);
            else
                return View(result);
        }

        public async Task<ActionResult> RoutesWithMaxStops(string inputGraph, string route, string maxStops)
        {
            var input = maxStops + "Y" + route + "Y" + inputGraph;
            var routeMapSrv = new RouteMapService();
            var result = await routeMapSrv.GetRoutesWithMaxStopsAsync(input);
            if (Request.IsAjaxRequest())
                return PartialView("_pwRoutesWithMaxStops", result);
            else
                return View(result);
        }

    }
}
