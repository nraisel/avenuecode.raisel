﻿using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc4_AvenueCode.Models
{
    public static class Util
    {
        public static string getServiceUri(string srv, string inputGraph)
        {
            return ConfigurationManager.AppSettings["RouteMapServiceURI"] + "api/" + srv + "/" + inputGraph;
        }
    }
}