﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mvc4_AvenueCode.Models
{
    public class TAnswer
    {
        public string route { get; set; }
        public int value { get; set; }
        public string response { get; set; }
    }
}