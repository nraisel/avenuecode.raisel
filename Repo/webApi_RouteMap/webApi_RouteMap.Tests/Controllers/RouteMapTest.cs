﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webApi_RouteMap.Models;

namespace webApi_RouteMap.Tests.Controllers
{
    [TestClass]
    public class RouteMapTest
    {
        [TestMethod]
        public void Test_to_get_total_distance_for_a_given_route()
        {
            // Arrange
            IRouteMap routeMapObj = new TRouteMap();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "E-A-B";
            string routeAndInputGraph = route + "Y" + inputGraph;

            // Act
            TAnswer result = routeMapObj.getTotalDistance_normalMode(routeAndInputGraph);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("29", result.response);
        }

        [TestMethod]
        public void Test_to_get_distance_between_two_towns_recursevly()
        {
            // Arrange
            TRouteMap routeMapObj = new TRouteMap();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-B-C-D";
            string routeAndInputGraph = route + "Y" + inputGraph;

            // Act
            TAnswer result = routeMapObj.getTotalDistance_recursively(routeAndInputGraph);

            // Assert
            Assert.IsNotNull(result);
            //Assert.AreEqual("-A-B-C", result.ElementAt(0));
            Assert.AreEqual("18", result.response);
        }

        [TestMethod]
        public void Test_to_get_routes_between_two_towns_recursively_with_a_maximum_of_stops()
        {
            // Arrange
            IRouteMap routeMapObj = new TRouteMap();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "D-E";
            int numberOfStops = 3;
            string routeAndInputGraph = numberOfStops.ToString() + "Y" + route + "Y" + inputGraph;
            
            // Act
            IEnumerable<string> result = routeMapObj.getRoutesWithMaxStops_recursively(0, routeAndInputGraph);

            // Assert
            Assert.IsNotNull(result);
            //Assert.AreEqual("-A-B-C", result.ElementAt(0));
            Assert.AreEqual(3, result.Count());
        }

        [TestMethod]
        public void Test_to_get_shortest_route_between_two_towns_recursevly()
        {
            // Arrange
            TRouteMap routeMapObj = new TRouteMap();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-C";
            string routeAndInputGraph = route + "Y" + inputGraph;

            // Act
            TAnswer result = routeMapObj.getShortestRoute(routeAndInputGraph);

            // Assert
            Assert.IsNotNull(result);
            //Assert.AreEqual("-A-B-C", result.ElementAt(0));
            //Assert.AreEqual("-A-B-C", result.route);
            //Assert.AreEqual(15, result.value);
            Assert.AreEqual("A-B-C", result.response);
        }


        [TestMethod]
        public void Test_to_get_distance()
        {
            // Arrange
            TRouteMap routeMapObj = new TRouteMap();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-D-E-B-C-D-E";
            string routeAndInputGraph = route + "Y" + inputGraph;

            // Act
            int result = routeMapObj.getTotalDistance(routeAndInputGraph);

            // Assert
            Assert.IsNotNull(result);
            //Assert.AreEqual("-A-B-C", result.ElementAt(0));
            //Assert.AreEqual("-A-B-C", result.route);
            //Assert.AreEqual(15, result.value);
            Assert.AreEqual(78, result);
        }
    }
}
