﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webApi_RouteMap.Models;

namespace webApi_RouteMap.Controllers
{
    public class RoutesWithMaxStopsController : ApiController
    {
        public IEnumerable<string> GetRoutesWithMaxStops(string routeGraph)
        {
            IRouteMap routeMap = new TRouteMap();
            return routeMap.getRoutesWithMaxStops_recursively(0, routeGraph);
        }
    }
}
