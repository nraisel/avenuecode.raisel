﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class THelper : IHelper, IInputGraph
    {
        /*Loop through the string input graph and extract all available nodes and put them on a list*/
        public IEnumerable<TNode> getNodeList(object inputGraph)
        {
            IList<TNode> result = new List<TNode>();
            IEnumerable<string> edgeArray = getInputGraphList(inputGraph);
            foreach (var item in edgeArray)
            {
                var edge = item.Trim().Substring(0, 2);
                var originNode_name = edge.Substring(0, 1);
                var destinationNode_name = edge.Substring(1, 1);
                var originNode = new TNode { name = originNode_name };
                var destinationNode = new TNode { name = destinationNode_name };
                if (!(result.Any(n => n.name == originNode_name)))
                    result.Add(originNode);

                if (!(result.Any(n => n.name == destinationNode_name)))
                    result.Add(destinationNode);
            }

            return result;
        }

        /*For every node in the NodeList, will get the list of nodes neighbors to it. this will
         return a list<TRoute> objects (Adjacent list for every node)*/
        public IEnumerable<TRoute> getNeighborList(object inputGraph)
        {
            IList<TRoute> result = new List<TRoute>();
            IEnumerable<string> edgeArray = getInputGraphList(inputGraph);
            IEnumerable<TNode> nodeList = getNodeList(inputGraph);
            var alistOfNodeNeighbors = new List<TEdge>();
            foreach (var node in nodeList)
            {
                alistOfNodeNeighbors.Clear();
                foreach (var edge in edgeArray)
                {
                    var ed = edge.Trim();
                    if (ed.Substring(0, 1) == ((TNode)node).name)
                    {
                        var destinationNode_name = ed.Substring(1, 1);
                        var destinationNode = new TNode { name = destinationNode_name };
                        var weight = int.Parse(ed.Substring(2, ed.Length - 2));
                        var edgeObj = new TEdge { destinationNode = destinationNode, weight = weight };
                        alistOfNodeNeighbors.Add(edgeObj);
                    }

                }
                var listOfTownsNeighbors = new List<TEdge>();
                foreach (var item in alistOfNodeNeighbors)
                    listOfTownsNeighbors.Add(item);
                var route = new TRoute { originNode = node, listOfNodeNeighbors = listOfTownsNeighbors };
                result.Add(route);
            }

            return result;
        }

        /*this will return an array of string. */
        public IEnumerable<string> getInputGraphList(object inputGraph)
        {
            return ((string)inputGraph).Replace(", ", ",").Split(',');
        }

        /*This method will get a list of Pair of nodes in the following mode:
         route = 'A-B-C-D' and it'll return List = 'AB', 'BC', 'CD' */
        public IEnumerable<string> getPairOfNodeList(string route)
        {
            IList<string> result = new List<string>();
            string formatedRoute = route.Replace("-", "");
            formatedRoute = formatedRoute.Trim();
            for (int i = 0; i <= formatedRoute.Length - 2; i++)
            {
                string origin = formatedRoute.ElementAt(i).ToString();
                string destiny = formatedRoute.ElementAt(i + 1).ToString();
                string edge = origin + destiny;
                result.Add(edge);
            }

            return result;
        }

        public bool existEdge(string edge, TRouteMapView routeMapView)
        {
            return routeMapView.inputGraphList.Any(e => e.Substring(0, 2) == edge);
        }

        public TAnswer getTotalDistance_normalMode(string route, IEnumerable<string> listofEdges, TRouteMapView routeMapView)
        {
            int distance = 0;
            foreach (var edge in listofEdges)
            {
                if (routeMapView.inputGraphList.Any(e => e.Substring(0, 2) == edge))
                {
                    int i = 0;
                    bool found = false;

                    while (i <= routeMapView.inputGraphList.Count() - 1 && !found)
                    {
                        var edgeOfRoute = routeMapView.inputGraphList.ElementAt(i).Trim().Substring(0, 2);
                        if (edge == edgeOfRoute)
                        {
                            distance += int.Parse(routeMapView.inputGraphList.ElementAt(i).Trim().Substring(2, routeMapView.inputGraphList.ElementAt(i).Trim().Length - 2));
                            found = true;
                        }
                        i++;
                    }
                }
                else
                    return new TAnswer { route = route, response = "NO SUCH ROUTE" };
            }

            return new TAnswer { route = route, response = distance.ToString() };
        }

        public IEnumerable<string> getRoutesWithMaxStops_recursively(int numberOfStops,
                                            string route,
                                            TRouteMapView routeMapView,
                                            string originalRoute,
                                            string path,
                                            IList<string> rRoutes,
                                            bool gotOut,
                                            int originalNumberOfStops)
        {
            string originNodeName = route.Substring(0, 1);
            if ((originNodeName != route.Substring(2, 1)) && numberOfStops > 0)
            {
                string newOrigin = routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                                       .listOfNodeNeighbors
                                                       .Where(lon => lon.destinationNode.visited == false)
                                                       .First()
                                                       .destinationNode.name;
                if (!gotOut)
                {
                    routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                                           .listOfNodeNeighbors
                                                           .Where(lon => lon.destinationNode.name == newOrigin)
                                                           .First()
                                                           .destinationNode.visited = true;

                }
                numberOfStops--;
                route = route.Replace(originNodeName, newOrigin);
                gotOut = true;
                path = path + "-" + originNodeName;
                return getRoutesWithMaxStops_recursively(numberOfStops, route, routeMapView, originalRoute, path, rRoutes, gotOut, originalNumberOfStops);
            }
            if (originNodeName == route.Substring(route.Length - 1))
                rRoutes.Add(path + "-" + originNodeName);
            //only will stop when all nodes neighbors to node origin are marked as visited

            int nodeNeighborsVisited = routeMapView.neighborList.Where(tn => tn.originNode.name == originalRoute.Substring(0, 1))
                                                                .First()
                                                                .listOfNodeNeighbors
                                                                .Where(lon => lon.destinationNode.visited == true)
                                                                .Count();
            int totalOfNodeNeighbors = routeMapView.neighborList.Where(tn => tn.originNode.name == originalRoute.Substring(0, 1))
                                                                .First()
                                                                .listOfNodeNeighbors
                                                                .Count();

            if (nodeNeighborsVisited < totalOfNodeNeighbors)
            {
                path = "";
                gotOut = false;
                numberOfStops = originalNumberOfStops;
                route = originalRoute;
                return getRoutesWithMaxStops_recursively(numberOfStops, route, routeMapView, originalRoute, path, rRoutes, gotOut, originalNumberOfStops);
            }
            else
            {
                if (rRoutes.Count == 0)
                    rRoutes.Add("NO SUCH ROUTE: " + originalRoute);
                return rRoutes;
            }
        }

        public TAnswer getShortestRoute(string route, TRouteMapView routeMapView, string originalRoute, string path, string current, int distance, IList<TAnswer> responseList)
        {
            if (current != route.Substring(1, 1))
            {
                string originNodeName = route.Substring(0, 1);
                var nodeNonVisitedByOriginNode = routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                                       .listOfNodeNeighbors
                                                       .Where(lon => lon.destinationNode.visited == false)
                                                       .FirstOrDefault();
                if (nodeNonVisitedByOriginNode == null)
                {
                    if (responseList.Count > 0)
                        return responseList.OrderBy(g => g.value).First();
                    else
                        return new TAnswer { route = originalRoute, value = 0, response = "NO SUCH ROUTE" };
                }
                current = nodeNonVisitedByOriginNode.destinationNode.name;
                distance += nodeNonVisitedByOriginNode.weight;
                if (routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                             .listOfNodeNeighbors
                                             .Count() > 1)
                {
                    if (current != route.Substring(1, 1) |
                        (current == route.Substring(1, 1) && responseList.Any(e => e.route.Replace("-", "").Contains(route))))
                    {

                        routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                                                                   .listOfNodeNeighbors
                                                                                   .Where(lon => lon.destinationNode.name == current)
                                                                                   .First()
                                                                                   .destinationNode.visited = true;
                    }
                }
                path = path + "-" + originNodeName;
                route = route.Substring(0, 1).Replace(originNodeName, current);
                route = route + originalRoute.Substring(1, 1);
                return getShortestRoute(route, routeMapView, originalRoute, path, current, distance, responseList);
            }
            route = path + "-" + originalRoute.Substring(1, 1);
            if (responseList.Any(e => e.route.Replace("-", "").Contains(route.Replace("-", ""))))
                return responseList.OrderBy(g => g.value).First();
            else
            {
                responseList.Add(new TAnswer { route = path + "-" + originalRoute.Substring(1, 1), value = distance });
                current = "";
                path = "";
                route = originalRoute;
                distance = 0;
                return getShortestRoute(route, routeMapView, originalRoute, path, current, distance, responseList);
            }
        }

        public TAnswer getTotalDistance_recursively(string route, TRouteMapView routeMapView, int distance, int pos)
        {
            if (pos <= route.Length - 2)
            {
                string originNodeName = route.Substring(pos, 1);
                string destinationNodeName = route.Substring(pos + 1, 1);
                if (existEdge(originNodeName + destinationNodeName, routeMapView))
                {
                    distance += routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                                         .listOfNodeNeighbors
                                                         .Where(lon => lon.destinationNode.name == destinationNodeName)
                                                         .First()
                                                         .weight;
                    pos++;
                    return getTotalDistance_recursively(route, routeMapView, distance, pos);
                }
                else
                {
                    pos = route.Length + 100;
                    distance = 0;
                }
            }
            string response = "NO SUCH ROUTE";
            if (distance != 0)
                response = distance.ToString();
            return new TAnswer { route = route, response = response };
        }

        public int getTotalDistance(string route, TRouteMapView routeMapView, int pos)
        {
            if (pos <= route.Length - 2)
            {
                string originNodeName = route.Substring(pos, 1);
                string destinationNodeName = route.Substring(pos + 1, 1);
                if (existEdge(originNodeName + destinationNodeName, routeMapView))
                {
                    pos++;
                    var distance = routeMapView.neighborList.Where(tn => tn.originNode.name == originNodeName).First()
                                                         .listOfNodeNeighbors
                                                         .Where(lon => lon.destinationNode.name == destinationNodeName)
                                                         .First()
                                                         .weight;
                    return distance + getTotalDistance(route, routeMapView, pos);
                }
                else
                {
                    pos = route.Length + 100;

                }
            }
            string response = "NO SUCH ROUTE";
            //return new TAnswer { route = route, response = response };
            return 0;
        }
    }
}