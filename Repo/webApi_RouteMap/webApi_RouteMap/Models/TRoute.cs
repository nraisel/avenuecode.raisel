﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class TRoute
    {
        public TNode originNode { get; set; }
        public IEnumerable<TEdge> listOfNodeNeighbors { get; set; }
    }
}