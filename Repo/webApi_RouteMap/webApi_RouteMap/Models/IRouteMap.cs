﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webApi_RouteMap.Models
{
    public interface IRouteMap
    {
        TAnswer getTotalDistance_normalMode(string routeGraph);
        TAnswer getTotalDistance_recursively(string routeGraph);
        TAnswer getShortestRoute(string routeGraph);
        IEnumerable<string> getRoutesWithMaxStops_recursively(int numberOfStops, string routeGraph);
    }
}
