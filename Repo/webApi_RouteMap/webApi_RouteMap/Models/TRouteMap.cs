﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webApi_RouteMap.Models
{
    public class TRouteMap : IRouteMap
    {
        public TAnswer getTotalDistance_normalMode(string routeGraph) 
        {
            IEnumerable<string> routeAndInputGraph = separateRouteAndInputGraph(routeGraph);
            IMap mapRoute = new TMapInit();
            TRouteMapView routeMapView = mapRoute.createRouteMap(routeAndInputGraph.ElementAt(1));
            THelper hlp = new THelper();
            IEnumerable<string> listofEdges = hlp.getPairOfNodeList(routeAndInputGraph.ElementAt(0));
            return hlp.getTotalDistance_normalMode(routeAndInputGraph.ElementAt(0), listofEdges, routeMapView);
        }

        public TAnswer getTotalDistance_recursively(string routeGraph)
        {
            IEnumerable<string> routeAndInputGraph = separateRouteAndInputGraph(routeGraph);
            IList<string> rRoutes = new List<string>();
            IMap mapRoute = new TMapInit();
            THelper hlp = new THelper();
            TRouteMapView routeMapView = mapRoute.createRouteMap(routeAndInputGraph.ElementAt(1));
            
            return hlp.getTotalDistance_recursively(routeAndInputGraph.ElementAt(0).Replace("-", ""), routeMapView, 0, 0);
        }

        public TAnswer getShortestRoute(string routeGraph)
        {
            IEnumerable<string> routeAndInputGraph = separateRouteAndInputGraph(routeGraph);
            int distance = 0;
            string path = "";
            IList<string> rRoutes = new List<string>();
            IMap mapRoute = new TMapInit();
            TRouteMapView routeMapView = mapRoute.createRouteMap(routeAndInputGraph.ElementAt(1));
            THelper hlp = new THelper();
            return hlp.getShortestRoute(routeAndInputGraph.ElementAt(0).Replace("-", ""), routeMapView, routeAndInputGraph.ElementAt(0).Replace("-", ""), path, "", distance, new List<TAnswer>());
        }

        public IEnumerable<string> getRoutesWithMaxStops_recursively(int numberOfStops, string routeGraph)
        {
            IEnumerable<string> routeAndInputGraph = separateRouteAndInputGraph(routeGraph);
            numberOfStops = int.Parse(routeAndInputGraph.ElementAt(0));
            string route = routeAndInputGraph.ElementAt(1);
            string path = "";
            IList<string> rRoutes = new List<string>();
            IMap mapRoute = new TMapInit();
            TRouteMapView routeMapView = mapRoute.createRouteMap(routeAndInputGraph.ElementAt(2));
            THelper hlp = new THelper();
            return hlp.getRoutesWithMaxStops_recursively(numberOfStops, route, routeMapView, route, path, rRoutes, false, numberOfStops);
        }

        private IEnumerable<string> separateRouteAndInputGraph(string routeGraph)
        {
            return routeGraph.Split('Y');
        }


        public int getTotalDistance(string routeGraph)
        {
            IEnumerable<string> routeAndInputGraph = separateRouteAndInputGraph(routeGraph);
            IList<string> rRoutes = new List<string>();
            IMap mapRoute = new TMapInit();
            THelper hlp = new THelper();
            TRouteMapView routeMapView = mapRoute.createRouteMap(routeAndInputGraph.ElementAt(1));

            return hlp.getTotalDistance(routeAndInputGraph.ElementAt(0).Replace("-", ""), routeMapView, 0);
        }
    }
}