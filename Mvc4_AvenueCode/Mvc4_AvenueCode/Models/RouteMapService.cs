﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Mvc4_AvenueCode.Models
{
    public class RouteMapService
    {
        public async Task<TAnswer> GetRouteTotalDistanceRecursivelyAsync(string inputGraph,
            // Implementation removed.
            CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = Util.getServiceUri("RouteTotalDistanceRecursively", inputGraph);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<TAnswer>());
            }
        }

        public async Task<TAnswer> GetShortestRouteBetweenTwoTownsAsync(string inputGraph,
            // Implementation removed.
            CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = Util.getServiceUri("ShortestRouteBetweenTwoTowns", inputGraph);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<TAnswer>());
            }
        }

        public async Task<IEnumerable<string>> GetRoutesWithMaxStopsAsync(string inputGraph,
            // Implementation removed.
            CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = Util.getServiceUri("RoutesWithMaxStops", inputGraph);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<IEnumerable<string>>());
            }
        }
    }
}